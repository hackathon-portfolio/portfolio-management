import './StockPortfolio.css';
import Table from 'react-bootstrap/Table'
import Header from '../header/Header'
import SubHeader from '../subheader/SubHeader'
import React from 'react'

class StockPortfolio extends React.Component {
  constructor(props) {
    super(props)
  }

  deriveStockData(data) {
    return data.map((stock) => {
        const { price, amount, currentPrice=4 } = stock
        const total = (price * amount).toFixed(2)
        const currentTotal = (currentPrice * amount).toFixed(2)
        const gain = (((total / currentTotal) - 1) * 100).toFixed(2)
        return { total, gain, ...stock }
    })
}

  render() {
    const { userName, balance, data } = this.props;
    const completeData = this.deriveStockData(data)
    console.log(completeData)
    return (
      <div className="Portfolio-container">
        <div className="Portfolio-header-container">
          <Header className="Portfolio-header">
              Stock Portfolio
              <SubHeader>
                  {userName}'s portfolio has ${balance} CAD.
              </SubHeader>
          </Header>
        </div>
        <div className="Portfolio-container">
          <Table striped bordered hover>
            <thead className="thead-light">
              <tr><th>Ticker</th><th>Price</th><th>Amount</th><th>Total</th><th>Gain</th></tr>
            </thead>
            <tbody>
            {completeData.map(({ id, ticker, price, amount, total, gain }) =>
              <tr key={id}>
                <td>{ticker}</td>
                <td>${price} CAD</td>
                <td>{amount}</td>
                <td>${total} CAD</td>
                <td className={(gain > 0)
                  ? "Portfolio-Cell-Positive"
                  : (gain < 0)
                    ? "Portfolio-Cell-Negative"
                    : ""}>{gain}%</td>
              </tr>
            )}
            </tbody>
          </Table>
        </div>
      </div>
    )
  }
}

export default StockPortfolio;
