import './SubHeader.css';

function SubHeader({ children }) {
  return (
    <div className="SubHeader">
      <header>
        {children}
      </header>
    </div>
  );
}

export default SubHeader;
