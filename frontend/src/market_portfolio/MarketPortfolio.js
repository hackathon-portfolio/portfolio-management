import React from 'react';
import Table from 'react-bootstrap/Table'
import Header from '../header/Header'
import './MarketPortfolio.css';
import TradeButtons from '../trade_buttons/TradeButtons'

class MarketPortfolio extends React.Component {
  constructor(props) {
    super(props);
  }

  
  render () {
    const { data } = this.props
    return (
      <div className="Portfolio-container">
        <div className="Portfolio-header-container">
          <Header className="Portfolio-header">
              Market
          </Header>
        </div>
        <div className="Portfolio-container">
          <Table className="table table-striped table-bordered table-hover">
            <thead className="thead-light">
              <tr><th>Ticker</th><th>Price</th><th>Volume</th></tr>
            </thead>
            <tbody>
            {data.map(({ id, ticker, price, volume }, index) => 
              <tr key={id}>
                <td>{ticker}</td>
                <td>${price} CAD</td>
                <td><span className="TradeButton-Cell-Container">{volume}{<TradeButtons />}</span></td>
              </tr>
            )}
            </tbody>
          </Table>
        </div>
      </div>
    )
  }
}

export default MarketPortfolio;
