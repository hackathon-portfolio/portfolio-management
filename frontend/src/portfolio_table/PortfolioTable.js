import React from 'react';
import StockPortfolio from "../stock_portfolio/StockPortfolio";
import MarketPortfolio from "../market_portfolio/MarketPortfolio";
import { getStockByID } from "../utils/utils"

class PortfolioTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "John",
      balance: 100,
      market: [
        {
            id: "12345ab",
            ticker: "TSLA",
            price: 4.39,
            volume: 3485583
        },
        {
            id: "38325",
            ticker: "AAPL",
            price: 123.45,
            volume: 495925
        },
        {
            id: "abc123",
            ticker: "NFLX",
            price: 104.04,
            volume: 595599
        },
        {
            id: "20059",
            ticker: "XEQT",
            price: 20.49,
            volume: 59395
        }
    ],
      stock: [{
                    id: "20059",
                    price: 30.49,
                    amount: 3
                },
                {
                    id: "abc123",
                    price: 58.02,
                    amount: 7
                }    
            ]
    }
  }

  addSymbolToStocks(stock, market) {
    return stock.map((stock) => ({ 
      id: stock?.id, ticker: getStockByID(stock?.id, market)?.ticker, ...stock
    }))
  }

  render() {   
    const { userName, balance, market, stock } = this.state; 
    const stocksWithSymbols = this.addSymbolToStocks(stock, market)

    return (
        <div>
          <StockPortfolio userName={userName} balance={balance} data={stocksWithSymbols}/>
          <MarketPortfolio data={market}/>
        </div>
    );
  }
}

export default PortfolioTable;