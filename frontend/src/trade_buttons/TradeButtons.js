import './TradeButtons.css';
import Button from 'react-bootstrap/Button'
import React, { useState } from 'react';
import BuyWindow from '../trade_window/BuyWindow'
import SellWindow from '../trade_window/SellWindow'

function TradeButtons() {
  const [buyOpen, setBuyOpen] = useState(false)
  const [sellOpen, setSellOpen] = useState(false)
  return (
    <div className="TradeButtonContainer">
      {buyOpen && <BuyWindow show={buyOpen} onHide={() => setBuyOpen(false)}/>}
      {sellOpen && <SellWindow show={sellOpen} onHide={() => setSellOpen(false)}/>}
      <Button variant={"success"} size="sm" onClick={() => setBuyOpen(true)}>
        {`BUY`}
      </Button>
      {' '}
      <Button variant={"danger"} size="sm" onClick={() => setSellOpen(true)}>
        {`SELL`}
      </Button>
    </div>
  );
}

export default TradeButtons;
