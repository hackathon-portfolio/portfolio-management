import './App.css';
import PortfolioTable from '../portfolio_table/PortfolioTable'

function App() {
  return (
    <div className="App">
      <header>
        Welcome to the Team3 Portfolio Manager
      </header>
      <div>
        <PortfolioTable />
      </div>
    </div>
  );
}

export default App;
