function getStockByID(id, market) {
    const stock = market.filter(({ id: marketId }) => id === marketId)
    return (stock?.[0] || null)
}

export { getStockByID }