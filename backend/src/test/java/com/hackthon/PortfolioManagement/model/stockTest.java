package com.hackthon.PortfolioManagement.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class stockTest {

    @BeforeEach
    public void runBeforeEachTest() {
        System.out.println("this runs first!!");
    }


    @Test
    public void testStockGettersSetters() {
        Stock testStock = new Stock();
        testStock.setId("12345");
        testStock.setStockTicker("AAA");
        testStock.setPrice(10.0);
        testStock.setVolume(11);


        assertEquals("12345", testStock.getId());
        assertEquals("AAA", testStock.getStockTicker());
        assertEquals(10.0,testStock.getPrice());
        assertEquals(11,testStock.getVolume());
    }
}


