package com.hackthon.PortfolioManagement;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackthon.PortfolioManagement.controller.StockController;
import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.service.StockServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@WebMvcTest(StockController.class)
public class stockControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private StockServiceInterface stockService;

    @Test
    public void testStockControllerFindAll() throws Exception {
        String testId = "1234";

        List<Stock> allStock = new ArrayList<Stock>();
        Stock testStock = new Stock();
        testStock.setId(testId);
        allStock.add(testStock);

        when(stockService.findAll()).thenReturn(allStock);

        this.mockMvc.perform(get("/api/v1/stock"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));

    }

    @Test
    public void testStockControllerFindById() throws Exception{
        String testId="1234";
        Stock testStock=new Stock();
        testStock.setId(testId);
        Optional<Stock> byId1=Optional.of(testStock);
        when(stockService.findById(testId)).thenReturn(byId1);

        this.mockMvc.perform(get("/api/v1/stock/" + testId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
  }

    @Test
    public void testStockControllerSave() throws Exception{
        String testId="1234";
        Stock testStock=new Stock();
        testStock.setId(testId);
        when(stockService.save(testStock)).thenReturn(testStock);

        this.mockMvc.perform(post("/api/v1/stock").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testStock)))
                .andDo(print())
                .andExpect(status().is(200));
                //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }
    @Test
    public void testStockControllerUpdate() throws Exception{
        String testId="1234";
       // String testId1="12345";
        Stock testStock=new Stock();
        testStock.setId(testId);

        when(stockService.findById(testId)).thenReturn(Optional.of(testStock));
        when(stockService.update(testStock,testId)).thenReturn(testStock);

        this.mockMvc.perform(put("/api/v1/stock/stocks/" + testId).contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testStock)))
                .andDo(print())
                .andExpect(status().is(204));
        //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }

    @Test
    public void testStockControllerDelete() throws Exception{
        String testId="1234";
        Stock testStock=new Stock();
        testStock.setId(testId);

        when(stockService.findById(testId)).thenReturn(Optional.of(testStock));
        when(stockService.update(testStock,testId)).thenReturn(testStock);

        this.mockMvc.perform(delete("/api/v1/stock/" + testId))
                .andDo(print())
                .andExpect(status().is(204));
        //.andExpect(MockMvcResultMatchers.content().string(containsString("\"id\":\"1234\"")));
    }
}
