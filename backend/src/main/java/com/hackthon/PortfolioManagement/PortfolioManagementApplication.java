package com.hackthon.PortfolioManagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortfolioManagementApplication {

	public static void main(String[] args) {
		System.out.println("Hello Stock App");
		SpringApplication.run(PortfolioManagementApplication.class, args);
	}

}
