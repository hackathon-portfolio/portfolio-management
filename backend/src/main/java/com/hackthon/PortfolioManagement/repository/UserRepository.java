package com.hackthon.PortfolioManagement.repository;


import com.hackthon.PortfolioManagement.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface UserRepository extends MongoRepository<User, String> {
}
