package com.hackthon.PortfolioManagement.service;

import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.model.User;

import java.util.List;
import java.util.Optional;

public interface UserServiceInterface {
    List<User> findAll();

    Optional<User> findById(String id);

    User save(User user);

    User update(User user,String id);

    void delete(String id);
}
