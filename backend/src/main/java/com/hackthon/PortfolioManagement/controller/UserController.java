package com.hackthon.PortfolioManagement.controller;

import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.model.User;
import com.hackthon.PortfolioManagement.service.StockServiceInterface;
import com.hackthon.PortfolioManagement.service.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserServiceInterface userService;

    /**
     * find all users
     * @return
     */
    @GetMapping
    public List<User> findAll() {
        return userService.findAll();
    }

    /**
     *
     * @param id get User object by id if exist
     * @return
     */
    @GetMapping("{id}")
    public ResponseEntity<User> findById(@PathVariable String id) {
        try{
            return new ResponseEntity<User>(userService.findById(id).get(), HttpStatus.OK);
        }
        catch (NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    /**
     *
     * @param user provide User object to create one in database
     * @return
     */
    @PostMapping
    public User save( @RequestBody User user ) {
        return userService.save(user);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<Object> update(@RequestBody User user, @PathVariable String id) {

        if (userService.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        userService.update(user,id);
        return ResponseEntity.noContent().build();
    }

    /**
     *
     * @param id delete User object from the database
     * @return
     */
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {

        if (userService.findById(id).isEmpty()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        userService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
