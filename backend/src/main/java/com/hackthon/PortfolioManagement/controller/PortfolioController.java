package com.hackthon.PortfolioManagement.controller;

import com.hackthon.PortfolioManagement.model.Portfolio;
import com.hackthon.PortfolioManagement.model.Stock;
import com.hackthon.PortfolioManagement.service.PortfolioServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/portfolio")
public class PortfolioController {
    @Autowired
    private PortfolioServiceInterface portfolioService;

    /**
     *
     * @return all portfolios
     */
    @GetMapping
    public List<Portfolio> findAll() {
        return portfolioService.findAll();
    }

    /**
     *
     * @param id provide portfolioID
     * @return Portfolio object if exist
     */
    @GetMapping("{id}")
    public ResponseEntity<Portfolio> findById(@PathVariable String id) {
        try{
            return new ResponseEntity<Portfolio>(portfolioService.findById(id).get(), HttpStatus.OK);
        }
        catch (NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    /**
     *
     * Used to create a new porfolio
     * @param portfolio provide Portfolio object
     * @return
     */

    @PostMapping
    public Portfolio save(@RequestBody Portfolio portfolio ) {
        return portfolioService.save(portfolio);
    }

    /**
     *
     * Used to update an existing with a portfolioId as a path variable
     * @param portfolio provide Portfolio object
     * @return
     */
    @PutMapping("/portfolios/{id}")
    public ResponseEntity<Object> update(@RequestBody Portfolio portfolio, @PathVariable String id) {

        if (portfolioService.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        portfolioService.update(portfolio,id);
        return ResponseEntity.noContent().build();
    }

    /**
     *
     * @param id delete Portfolio object from the database
     * @return
     */
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {

        if (portfolioService.findById(id).isEmpty()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        portfolioService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
