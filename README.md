## Technical Getting Started Checklist
Create a spring boot project including web & spring-data-mongodb (you might also include lombok if you want to use it).

Create a bitbucket repository.

Add, commit, push your spring-boot project to the bitbucket repository.

Ensure your team has access to the bitbucket repository.

Decide on the absolute MINIMUM fields for a first working system e.g. the first version of your model object may just be: String id, String stockTicker, double price, int volume.

If you don't have the above completed in the first 90mins, contact your instructor for help.

