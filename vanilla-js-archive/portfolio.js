const PORTFOLIO_SERVICE_URL = 'http://localhost:8080/api/v1/stock'
const LOCAL_STORAGE = true
const PORTFOLIO_SERVICES = {
    EQUITY: 'equity',
    USER: 'user'
}

function retrievePortfolio() {
    market = null
    fetchPortfolio(PORTFOLIO_SERVICES['EQUITY']).then((json) => {
        market = json
        updateMarket(market)
    })
    fetchPortfolio(PORTFOLIO_SERVICES['USER']).then((json) => {
        updatePortfolio(json, market)
    })
}

async function fetchPortfolio(service) {
    /**if using local storage then return the saved data, 
     * otherwise access directly from the service.
     */
    return (
        LOCAL_STORAGE
            ? (await fetch('data.json').then(response => response.json()))[service]
            : await fetch(`${PORTFOLIO_SERVICE_URL}/${service}`).json()
    )
}

function updatePortfolio({ name, balance, equity }, market) {
    updatePortfolioBanner({ name, balance })
    equity = updateEquityObjects(equity, market)
    updatePortfolioTable(equity)
}

function updatePortfolioBanner({ name, balance }) {
    const nameBanner = document.getElementById("portfolio-banner")
    nameBanner.innerHTML = `${name}'s Portfolio`
    const balanceBanner = document.getElementById("portfolio-balance")
    balanceBanner.innerHTML = `You have $${balance} CAD available to purchase stocks.`
}

function updateEquityObjects(equity, markets) {
    /** update portfolio data
     *   - price
     *   - gain/loss
     */
    return equity.map(({ id, price, amount }) => {
        ticker = getMarketProperty(id, markets, 'stockTicker')
        total = `$${(price * amount).toFixed(2)} CAD`
        gainPercentage = ((getMarketProperty(id, markets, 'price') / price) - 1).toFixed(2)
        gain = `${gainPercentage}%`
        isPositive = gainPercentage >= 0
        return { id, ticker, price, amount, total, gain, isPositive }
    })
}

function getMarketProperty(id, markets, property) {
    /** from market data access the specified property's value */
    console.log(markets.find(stock => stock.id === id))
   return markets.find(stock => stock.id === id)[property];
}

function updatePortfolioTable(portfolio) {
    const table = document.getElementById("portfolio-table")
    populateTable(table, portfolio, true)
}

function updateMarket(market) {
    const table = document.getElementById("market-table")
    populateTable(table, market, false)
}

function getSellTableButtons({ id, ticker}) {
    return (
        `<div class="button-container">
            <button type="button" id = "${id}" onclick=sellStock("${id}","${ticker}") class="btn btn-danger">Sell</button>
        </div>`
    )
}

function getBuyTableButtons({ id,stockTicker,price}) {
    return (
        `<div class="button-container">
            <button type="button"  id="${id}" onclick=buyStock("${id}","${stockTicker}","${price}") class="btn btn-success">Buy</button>
        </div>`
    )
}

function populateTable(table, items, isPortfolio) {
    table && items.forEach(({ id, isPositive, ...props }) => {
        // add row to portfolio table
        let row = table.insertRow()
        if (isPortfolio) {
            row.style.backgroundColor = (
                isPositive
                    ? "rgb(34, 139, 34, 0.6)"
                    : "rgb(178, 34, 34, 0.6)"
            )
        }
        // set stock id to label in HTML
        row.id = id
        // generate the row HTML which includes buy/sell button
        Object.values(props).forEach((item, index) => {
            let cell = row.insertCell(index)
            cell.innerHTML = item 
            if(index == Object.values(props).length - 1) {
                cell.innerHTML += (isPortfolio
                    ? getSellTableButtons({ id,...props })
                    : getBuyTableButtons({ id, ...props})
                )
            }
        })
    });
}

// TODO: add validation logic for Sell order
function validateSell(name, amt){
    return true
}

// TODO: add validation logic for Buy order
function validateBuy(name, amt){
    return true
}

function executeTrade(id,ticker,amt,isBuy,price){
    fetchPortfolio(PORTFOLIO_SERVICES['USER']).then(({name,balance,equity}) => {
        // change equity field to reflect trade
        tickerObj = equity.find(item => item.id === id);
        if(tickerObj === undefined){
            console.log('tickerObj is undefined in equity')
            newObj = {}
            newObj.id = id
            newObj.price = price
            newObj.amount = amt
            equity.push(newObj)
            
        } else {

            tickerObj.amount += isBuy ? amt : -1*amt

            if(tickerObj.amount == 0 ){
                //remove from equity
                console.log('remove from equity')
                equity = equity.filter(function(value){
                    //console.log(value)
                    return value != tickerObj
                })
                console.log(equity)
            }
        }
        updatePortfolio({name,balance,equity},market)
    })

}

function buyStock(id, ticker,price) {
    console.log("buy id :" +id)
    console.log("buy ticker :" + ticker)
    // prompt user for amount to buy
    var amt = window.prompt("Amount to Buy of: " + ticker)
    console.log('buy ' + amt +' ' + ticker)

    // validate trade order
    valid = validateBuy(ticker,amt)
    err = "invalid Buy"

    // execute trade order
    if(valid){
        executeTrade(id,ticker,amt,true,price)
    } else {
        window.alert(err)
    }
}

function sellStock( id, ticker) {
    console.log('ticker :' + ticker)
    // prompt user for amount to Sell
    var amt = window.prompt("Amount to Sell of: " + ticker)
    console.log('sell '+amt+' '+ ticker)

    // validate Sell order
    valid= validateSell(id,amt);
    err = "invalid sell"

    // execute trade 
    if(valid){
        executeTrade(id,ticker,amt,false)
    } else {
        window.alert(err)
    }
}

retrievePortfolio();